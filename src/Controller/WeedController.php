<?php

namespace App\Controller;

use App\Entity\Weed;
use App\Form\WeedType;
use App\Repository\WeedRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/weed')]
class WeedController extends AbstractController
{
    #[Route('/', name: 'app_weed_index', methods: ['GET'])]
    public function index(WeedRepository $weedRepository): Response
    {
        return $this->render('weed/index.html.twig', [
            'weeds' => $weedRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_weed_new', methods: ['GET', 'POST'])]
    public function new(Request $request, WeedRepository $weedRepository): Response
    {
        $weed = new Weed();
        $form = $this->createForm(WeedType::class, $weed);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $weedRepository->add($weed);
            return $this->redirectToRoute('app_weed_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('weed/new.html.twig', [
            'weed' => $weed,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_weed_show', methods: ['GET'])]
    public function show(Weed $weed): Response
    {
        return $this->render('weed/show.html.twig', [
            'weed' => $weed,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_weed_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Weed $weed, WeedRepository $weedRepository): Response
    {
        $form = $this->createForm(WeedType::class, $weed);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $weedRepository->add($weed);
            return $this->redirectToRoute('app_weed_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('weed/edit.html.twig', [
            'weed' => $weed,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_weed_delete', methods: ['POST'])]
    public function delete(Request $request, Weed $weed, WeedRepository $weedRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$weed->getId(), $request->request->get('_token'))) {
            $weedRepository->remove($weed);
        }

        return $this->redirectToRoute('app_weed_index', [], Response::HTTP_SEE_OTHER);
    }
}

<?php

namespace App\Controller;

use App\Entity\Animal;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestAnimalController extends AbstractController
{
    #[Route('/test/animal', name: 'app_test_animal')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $animal = new Animal();
        $animal->setTitle('Chat');
        $animal->setDescription('jolie chatounou à son pepere');
        $animal->setLatin('felis caninis');
        $animal->setCreatedAt( new \DateTimeImmutable());

        $entityManager = $doctrine->getManager();
        $entityManager->persist($animal);
        $entityManager->flush();


        dd($animal);


        return $this->render('test_animal/index.html.twig', [

        ]);
    }
}

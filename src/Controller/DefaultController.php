<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

//#[Route('/admin')]
class DefaultController extends AbstractController
{
    #[Route('/', name: 'homepage')]
    public function index(): Response
    {
        $message = 'Ici message très important';
        $fruits = array('Banane','Kiwi','Papaye');
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'mes' => $message,
            'fruits' => $fruits,
        ]);
    }

    #[Route('/test1', name: 'test1')]
    public function test1(): Response
    {
        //return new Response('<h2>Voici votre reponse</h2>');
        $fruits = array('Banane','Kiwi','Papaye');
        //return new Response(json_encode($fruits), 200, ['Content-type' => 'application-json']);
        return new JsonResponse($fruits);

    }

    #[Route('/test2', name: 'test2')]
    public function test2(Request $request): Response
    {
        echo $request->server->get('SERVER_NAME');
        dd($request);
    }

    #[Route('/test3', name: 'test3',priority: 1, methods: ['GET','POST', 'PUT', 'PATCH', 'DELETE'])]
    public function test3(): Response
    {
        //return $this->redirectToRoute('test4', ['id' => 57, 'slug' => 'dedefaitduski']);
        // return $this->redirect('https://weblitzer.fr');
        //$url = $this->generateUrl('homepage'); // relative
//        $url = $this->generateUrl('homepage', [], UrlGeneratorInterface::ABSOLUTE_URL); // absolute
//        dd($url);

        // throw $this->createNotFoundException('non pas par là');
        $emailAdmin = $this->getParameter('app.adminemail');
        dd($emailAdmin);
    }

    #[Route('/test4/{id}/{slug}',
        name: 'test4',
        requirements: ['id' => '\d+', 'slug' => '[a-zA-Z]+'],
        methods: ['GET'], priority: 3
    )]
    public function test4($id,$slug): Response
    {
        dd(array($id,$slug));
    }



    public function getTheNameOfTheAuthor()
    {
        $emailAdmin = $this->getParameter('app.adminemail');
        return $this->render('partials/nameofauthor.html.twig', array(
            'email' => $emailAdmin
        ));
    }

}

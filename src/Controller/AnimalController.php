<?php

namespace App\Controller;

use App\Entity\Animal;
use App\Form\AnimalType;
use App\Repository\AnimalRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AnimalController extends AbstractController
{
    #[Route('/animals', name: 'app_animal')]
    public function index(AnimalRepository $repoAnimal): Response
    {
        $animals = $repoAnimal->findAll();
        return $this->render('animal/index.html.twig', [
            'animals' => $animals
        ]);
    }

    #[Route('/animal/{id}', name: 'app_single_animal', requirements: ['id' => '\d+'] )]
    public function single($id, AnimalRepository $repoAnimal): Response
    {
        $animal = $repoAnimal->find($id);
        if(!$animal) {
            throw $this->createNotFoundException('Animal not exist');
        }
        return $this->render('animal/single.html.twig', [
            'animal' => $animal
        ]);
    }

    #[Route('/animal/add', name: 'app_add_animal' )]
    public function add(Request $request,AnimalRepository $repoAnimal): Response
    {
        $animal = new Animal();
        $form = $this->createForm(AnimalType::class, $animal);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $animal->setCreatedAt(new \DateTimeImmutable());
            $repoAnimal->add($animal);
            $this->addFlash('success', 'Merci pour l\'ajout d\'un animal');
            return $this->redirectToRoute('app_animal');
        }
        return $this->render('animal/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route('/animal/edit/{id}', name: 'app_edit_animal', requirements: ['id' => '\d+'] )]
    public function edit($id,Request $request,AnimalRepository $repoAnimal): Response
    {
        $animal = $repoAnimal->find($id);
        if(!$animal) {
            throw $this->createNotFoundException('Animal not exist');
        }
        $form = $this->createForm(AnimalType::class, $animal);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $repoAnimal->add($animal);
            $this->addFlash('success', 'Merci pour l\'edit d\'un animal');
            return $this->redirectToRoute('app_animal');
        }
        return $this->render('animal/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }


}

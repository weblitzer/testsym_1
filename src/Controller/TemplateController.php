<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TemplateController extends AbstractController
{
    #[Route('/template', name: 'app_template')]
    public function index(): Response
    {
        $user = array(
            'nom' => 'Quidel',
            'email' => 'quidelantoine@gmail.com'
        );
        return $this->render('template/index.html.twig', array(
            'user' => $user
        ));
    }
}

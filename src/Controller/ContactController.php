<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    #[Route('/contact', name: 'app_contact')]
    public function index(Request $request): Response
    {
        $form = $this->createFormBuilder()
            ->add('nom', TextType::class, array(
                'label' => 'Votre nom',
                'label_attr' => ['class' => 'labelmaster'],
                'help' => 'ici votre nom si cela est pas claire ;)',
                'required' => false,
//                'disabled' => true,
                'attr' => [
                    'placeholder' => 'ici ton nom',
                    'class'  => 'inputform'
                ],
                'row_attr' => [
                    'class' => 'bigbox'
                ],
                'data' => 'jacky'
            ))
            ->add('message', TextareaType::class)
            ->add('fruits', ChoiceType::class, array(
                'choices'  => [
                    'Banane' => 'banane',
                    'Kiwi' => 'kiwi',
                    'Papaye' => 'papaye',
                ],
                'expanded' => true,
                'multiple' => true
            ))
            ->add('pays', CountryType::class, array(
                'preferred_choices' => array('FR')
            ))
            ->add('submit', SubmitType::class)
//            ->setMethod('GET')
//                ->setAction('')
            ->getForm();
        // handle
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
//            dd($request->request);
            dd($form->getData());
        }

        return $this->render('contact/index.html.twig', array(
            'formulaire' => $form->createView()
        ));
    }
}

<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class TestFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('entreprise', TextType::class, array(
                'label' => 'Votre entreprise'
            ))
            ->add('message', TextareaType::class, array(
                'constraints' => [
                    new NotBlank(),
                    // new NotBlank(['message' => 'Votre message est vide']),
                    new Length([
                        'min' => 3,
                        'max' => 10,
                        'minMessage' => 'Votre message est trop court min {{ limit }} characters.',
                        'maxMessage' => 'Votre message est trop long max {{ limit }} characters.',
                    ])
                ],
                'required' => false
            ))
            ->add('legal', CheckboxType::class, array(
                'required' => false,
                'mapped' => false
            ))
            ->add('dateperso', DateType::class, array(
                'widget' => 'single_text'
            ))
            ->add('submit', SubmitType::class)
        ;

    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
